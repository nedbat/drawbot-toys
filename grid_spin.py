import os

canvas_h = 300
canvas_w = canvas_h * 2
n_frames = 60
grid = 10

def new_page():
    newPage(canvas_w, canvas_h)
    frameDuration(1/50)
    fill(0)
    rect(0, 0, canvas_w, canvas_h)
    newPath()

def draw_grid(xc, yc, grid, rot, extent):
    save()
    translate(xc, yc)
    rotate(rot)
    for x in range(-extent, extent, grid):
        moveTo((x, -extent))
        lineTo((x, extent))
        
    for y in range(-extent, extent, grid):
        moveTo((-extent, y))
        lineTo((extent, y))
    stroke(1)
    drawPath()
    restore()

def clip_rect(llx, lly, urx, ury):
    moveTo((llx, lly))
    lineTo((urx, lly))
    lineTo((urx, ury))
    lineTo((llx, ury))
    closePath()
    clipPath()
    newPath()
    
half = canvas_h//2
extent = canvas_h

for frame in range(n_frames):
    new_page()
    rot = frame * 90/n_frames
    save()
    clip_rect(0, 0, canvas_w//2, canvas_h)
    draw_grid(half, half, grid, rot, extent)
    restore()
    
    save()
    clip_rect(canvas_w//2, 0, canvas_w, canvas_h)
    draw_grid(canvas_w//2 + half, half, grid, rot*2, extent)
    restore()

name = os.path.splitext(os.path.basename(__file__))[0]
saveImage("~/Downloads/%s.gif" % name)