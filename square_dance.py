# Ported from https://nedbatchelder.com/blog/200208/obfuscated_code.html

import contextlib
import os

def new_page():
    newPage(canvas_size, canvas_size)
    frameDuration(frame_duration)
    fill(0)
    rect(0, 0, canvas_size, canvas_size)
    newPath()

@contextlib.contextmanager
def stroked_path(*color):
    save()
    newPath()
    try:
        yield
    finally:
        stroke(*color)
        fill(None)
        drawPath()
        restore()
        
def grid_range(start, stop):
    v = start - grid * 2
    while v < stop + grid * 2:
        yield v
        v += grid * 2

def magic(d):
    rfcg = D * grid
    return rfcg * .6, rfcg * 1.2, rfcg * -.6, rfcg * -1.2
    
def square_dance():
    fa = 0
    for across in grid_range(0, canvas_size):
        with stroked_path(1):
            translate(across, fa-grid)
            moveTo((0, 0))
            for i, along in enumerate(grid_range(0, canvas_size)):
                a, b, c, d = magic(along + grid)
                lineTo((c, d+i*grid*2))
                lineTo((a, b+i*grid*2))

        with stroked_path(1):
            translate(across+grid, fa-2*grid)
            moveTo((0, 0))
            for i, along in enumerate(grid_range(0, canvas_size)):
                a, b, c, d = magic(along)
                lineTo((b, c+i*grid*2))
                lineTo((d, a+i*grid*2))

    for along in grid_range(0, canvas_size):
        along += grid
        with stroked_path(1):
            translate(grid*-2, along)
            moveTo((0, 0))
            for i, across in enumerate(grid_range(0, canvas_size)):
                a, b, c, d = magic(along)
                lineTo((c+i*grid*2, d))
                lineTo((a+i*grid*2, b))
                
        with stroked_path(1):
            translate(-grid, along-grid)
            moveTo((0, 0))
            for i, across in enumerate(grid_range(0, canvas_size)):
                a, b, c, d = magic(along - grid)
                lineTo((d+i*grid*2, a))
                lineTo((b+i*grid*2, c))

def one_and_back(v):
    """Convert a 0..1 value to 0..1..0"""
    return 2 * (0.5 - abs(v - 0.5))

canvas_size = 500
n_frames = 40
grid = 75
frame_duration = 1/20

for frame in range(n_frames):
    new_page()
    D = one_and_back(frame / n_frames)
    save()
    square_dance()

name = os.path.splitext(os.path.basename(__file__))[0]    
saveImage("~/Downloads/%s.gif" % name)